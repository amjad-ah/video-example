# Video chat example

> step 1:

``` bash
$ git clone https://bitbucket.org/amjad-ah/video-example.git
```

> step 2:

copy `.env.example` to `.env` and edit the environment variables inside

> step 3:

``` bash
$ npm install
```

> step 4:

``` bash
$ npm start
```

> step 5:

Go to <http://localhost:3000/index.html>

> step 6:

enter the passcode you created inside the `.env` file then join the room